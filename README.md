* Run migrations and seeds
```
php artisan migrate:fresh --seed
```
* Setup your domains list in **database/seeders/DatabaseSeeder.php**
* Create the symbolic link
```
php artisan storage:link
```
* Start the server
* Register user **yourdomain**/admin/register
* Then you can create landing page
* When landing page was created, edit page will always open instead
