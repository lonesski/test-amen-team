<?php

use App\Http\Controllers\LandingPageHandlerController;
use Illuminate\Support\Facades\Route;

Route::get('/', [LandingPageHandlerController::class, 'index']);
