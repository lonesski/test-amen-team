<?php

// Admin routes

use App\Http\Controllers\Admin\AuthController;
use App\Http\Controllers\Admin\LandingPageController;

Route::middleware('guest')->group(function () {
    Route::get('login', [AuthController::class, 'showLoginForm'])->name('login');
    Route::post('login', [AuthController::class, 'login']);

    Route::get('register', [AuthController::class, 'showRegistrationForm'])->name('register');
    Route::post('register', [AuthController::class, 'register']);
});
Route::post('logout', [AuthController::class, 'logout'])->name('logout')->middleware('auth:web');

Route::middleware('auth:web')->name('admin.')->group(function () {
    Route::get('/', [LandingPageController::class, 'index'])->name('dashboard');

    Route::prefix('landing')->name('landing.')->group(function () {
        Route::post('/', [LandingPageController::class, 'store'])->name('store');
        Route::put('/', [LandingPageController::class, 'update'])->name('update');
    });
});

