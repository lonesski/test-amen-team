<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;

class User extends Authenticatable
{
    use HasFactory, Notifiable;

    protected $fillable = [
        'name',
        'email',
        'password',
        'birth_date',
        'avatar_url',
    ];

    protected $hidden = [
        'password',
    ];

    protected $casts = [
        'birth_date' => 'date',
    ];

    public function domain()
    {
        return $this->hasOne(Domain::class);
    }

    public function landing_page()
    {
        return $this->hasOneThrough(LandingPage::class, Domain::class);
    }

    public function setDomain(int $domainId)
    {
        Domain::query()->whereKey($domainId)->update(['user_id' => auth()->id()]);
    }
}
