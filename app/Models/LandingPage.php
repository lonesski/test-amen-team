<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class LandingPage extends Model
{
    protected $fillable = [
        'title',
        'sub_title',
        'img_path',
        'content',
        'domain_id',
        'template',
    ];

    public function domain()
    {
        return $this->belongsTo(Domain::class);
    }

    public static function findByHost(string $host)
    {
        return self::query()
                   ->whereHas('domain', function (Builder $q) use ($host) {
                       $q->where('host', $host);
                   })
                   ->first();
    }
}