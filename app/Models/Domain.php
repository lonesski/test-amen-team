<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class Domain extends Model
{
    public $timestamps = false;

    protected $fillable = ['host'];

    public function scopeFree(Builder $q)
    {
        $q->whereNull('user_id');
    }

    public function isFree(): bool
    {
        return is_null($this->user_id);
    }
}