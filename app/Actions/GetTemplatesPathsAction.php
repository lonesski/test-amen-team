<?php

namespace App\Actions;

class GetTemplatesPathsAction
{
    public function run()
    {
        return collect(\File::files(resource_path('views/admin/templates')))
            ->map(function (\SplFileInfo $file) {
                if ($path = str_replace('.blade.php','',$file->getBasename())) {
                    return $path;
                }
            })
            ->filter();
    }
}