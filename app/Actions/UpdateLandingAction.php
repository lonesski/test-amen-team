<?php

namespace App\Actions;

use App\Helpers\FileUploadHelper;

class UpdateLandingAction
{
    public function run(array $data)
    {
        if (\Arr::has($data, 'img_path')) {
            $data['img_path'] = FileUploadHelper::uploadFileOrReturnPath($data['img_path'], 'landing');
        }
        auth()->user()->landing_page->update($data);
    }
}