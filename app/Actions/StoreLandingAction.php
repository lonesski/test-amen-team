<?php

namespace App\Actions;

use App\Helpers\FileUploadHelper;
use App\Models\Domain;
use Illuminate\Validation\ValidationException;

class StoreLandingAction
{
    public function run(array $data)
    {
        $this->validateDomain($data);

        if (!auth()->user()->landing_page) {
            $data['img_path'] = FileUploadHelper::uploadFileOrReturnPath($data['img_path'], 'landing');

            auth()->user()->setDomain($data['domain_id']);
            auth()->user()->landing_page()->create($data);
        }
    }

    public function validateDomain(array $data): void
    {
        if (!Domain::find($data['domain_id'])->isFree()) {
            throw ValidationException::withMessages([
                'domain_id' => 'Domain is busy'
            ]);
        }
    }
}