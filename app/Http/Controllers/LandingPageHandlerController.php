<?php

namespace App\Http\Controllers;

use App\Models\LandingPage;
use Illuminate\Http\Request;

class LandingPageHandlerController extends Controller
{
    public function index(Request $request)
    {
        $landing = LandingPage::findByHost($request->getHost());

        if (!$landing) {
            return response("There is no landing yet");
        }
        return view('admin.templates.' . $landing->template, [
            'landing' => $landing
        ]);
    }
}