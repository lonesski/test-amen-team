<?php

namespace App\Http\Controllers\Admin;

use App\Actions\GetTemplatesPathsAction;
use App\Actions\StoreLandingAction;
use App\Actions\UpdateLandingAction;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\StoreLandingPageRequest;
use App\Http\Requests\Admin\UpdateLandingPageRequest;
use App\Models\Domain;

class LandingPageController extends Controller
{
    public function index()
    {
        $landing = auth()->user()->landing_page;
        $templates = app(GetTemplatesPathsAction::class)->run();

        if ($landing) {
            return view('admin.landing.edit', [
                'landing'   => $landing,
                'templates' => $templates,
            ]);
        }

        return view('admin.landing.create', [
            'domains'   => $landing ? [] : Domain::free()->get(),
            'templates' => $templates,
        ]);
    }

    public function store(StoreLandingPageRequest $request)
    {
        app(StoreLandingAction::class)->run($request->validated());

        return \Redirect::route('admin.dashboard');
    }

    public function update(UpdateLandingPageRequest $request)
    {
        app(UpdateLandingAction::class)->run($request->validated());

        return \Redirect::route('admin.dashboard');
    }
}