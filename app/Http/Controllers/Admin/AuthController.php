<?php

namespace App\Http\Controllers\Admin;

use App\Helpers\FileUploadHelper;
use App\Http\Controllers\Controller;
use App\Http\Requests\Admin\LoginRequest;
use App\Http\Requests\Admin\RegisterRequest;
use App\Models\User;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class AuthController extends Controller
{
    public function showLoginForm()
    {
        return view('auth.login');
    }

    public function login(LoginRequest $request)
    {
        if (Auth::attempt($request->only(['email', 'password']))) {
            return \Redirect::route('admin.dashboard');
        }
        throw ValidationException::withMessages([
            'email' => 'Password is wrong'
        ]);
    }

    public function showRegistrationForm()
    {
        return view('auth.register');
    }

    public function register(RegisterRequest $request)
    {
        $data = $request->validated();
        $data['avatar_url'] = FileUploadHelper::uploadFileOrReturnPath($data['avatar_url'], 'avatar');
        $data['password'] = \Hash::make($request->get('password'));

        $user = User::create($data);

        Auth::loginUsingId($user->id);

        return \Redirect::route('admin.dashboard');
    }

    public function logout()
    {
        Auth::logout();

        return \Redirect::route('login');
    }
}