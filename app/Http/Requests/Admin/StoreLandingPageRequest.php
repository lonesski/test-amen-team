<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class StoreLandingPageRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title'      => 'required|string',
            'sub_title'  => 'required|string',
            'img_path'   => 'required',
            'content'    => 'required|string',
            'domain_id'  => 'required|exists:domains,id',
            'template'   => 'required|string',
        ];
    }
}