<?php

namespace App\Http\Requests\Admin;

use Illuminate\Foundation\Http\FormRequest;

class UpdateLandingPageRequest extends FormRequest
{
    public function rules()
    {
        return [
            'title'      => 'required|string',
            'sub_title'  => 'required|string',
            'img_path'   => 'nullable',
            'content'    => 'required|string',
            'template'   => 'required|string',
        ];
    }
}