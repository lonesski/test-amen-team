<?php

namespace App\Helpers;

use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Storage;

class FileUploadHelper
{
    public static function uploadFileOrReturnPath($value, $path): ?string
    {
        if ($value instanceof UploadedFile) {
            return Storage::url($value->store($path));
        }
        if (is_string($value)) {
            return $value;
        }
        return null;
    }
}