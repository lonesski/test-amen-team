<html>
<head>
    <title>{{ $landing->title }}</title>
</head>
<body>
<h1>Template 3</h1>

<h2>{{ $landing->sub_title }}</h2>

<img src="{{ $landing->img_path }}" height="400">

<div>
    {!! $landing->content !!}
</div>
</body>
</html>