<html>
<head>
    <title>{{ $landing->title }}</title>
</head>
<body>
<h1>Template 1</h1>

<h2>{{ $landing->sub_title }}</h2>

<img src="{{ $landing->img_path }}" height="200">

<div style="border: solid 2px; padding: 10px">
    {!! $landing->content !!}
</div>
</body>
</html>