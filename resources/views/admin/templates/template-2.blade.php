<html>
<head>
    <title>{{ $landing->title }}</title>
</head>
<body>
<h1>Template 2</h1>

<h2>{{ $landing->sub_title }}</h2>

<img src="{{ $landing->img_path }}" height="300">

<div style="border: dotted 4px; padding: 20px">
    {!! $landing->content !!}
</div>
</body>
</html>