@extends('adminlte::page')

@section('title') Create landing @endsection

@section('plugins.Summernote', true)
@section('plugins.BsCustomFileInput', true)

@section('content')
    @if(count($domains) == 0)
        There is no available domains
    @else
        <form action="{{route('admin.landing.store')}}" method="post" enctype="multipart/form-data">
            @csrf
            <x-adminlte-select name="domain_id" label="Domain">
                @foreach($domains as $domain)
                    <option value="{{$domain->id}}">{{$domain->host}}</option>
                @endforeach
            </x-adminlte-select>

            <x-adminlte-select name="template" label="Template">
                @foreach($templates as $template)
                    <option value="{{$template}}">{{$template}}</option>
                @endforeach
            </x-adminlte-select>

            <x-adminlte-input name="title" label="Title"/>

            <x-adminlte-input name="sub_title" label="Sub title"/>

            <x-adminlte-input-file name="img_path" label="Image"/>

            <x-adminlte-text-editor name="content">
                <b>Lorem ipsum dolor sit amet</b>, consectetur adipiscing elit.
                <br>
                <i>Aliquam quis nibh massa.</i>
            </x-adminlte-text-editor>

            <x-adminlte-button label="Create" type="submit"/>
        </form>
    @endif
@endsection