@extends('adminlte::page')

@section('title') Edit landing @endsection

@section('plugins.Summernote', true)
@section('plugins.BsCustomFileInput', true)

@section('content')
    <form action="{{route('admin.landing.update')}}" method="post" enctype="multipart/form-data">
        @csrf
        @method('put')

        <x-adminlte-input name="domain_id" disabled label="Domain" value="{{$landing->domain->host}}"/>
        <x-adminlte-select name="template" label="Template">
            @foreach($templates as $template)
                <option value="{{$template}}"
                        @if($template == $landing->template) selected @endif
                >{{$template}}</option>
            @endforeach
        </x-adminlte-select>

        <x-adminlte-input name="title" label="Title" value="{{$landing->title}}"/>

        <x-adminlte-input name="sub_title" label="Sub title" value="{{$landing->sub_title}}"/>

        <x-adminlte-input-file name="img_path" label="Image" placeholder="{{$landing->img_path}}"/>

        <x-adminlte-text-editor name="content">
            {!! $landing->content !!}
        </x-adminlte-text-editor>

        <x-adminlte-button label="Update" type="submit"/>
    </form>
@endsection