@extends('adminlte::auth.auth-page', ['auth_type' => 'register'])

@section('auth_header', __('adminlte::adminlte.register_message'))

@section('plugins.BsCustomFileInput', true)

@section('auth_body')
    <form action="{{ route('register') }}" method="post" enctype="multipart/form-data">
        @csrf
        <x-adminlte-input name="name" label="Name"/>
        <x-adminlte-input name="email" label="Email" type="email"/>
        <x-adminlte-input name="password" label="Password" type="password"/>
        <x-adminlte-input name="birth_date" label="Birth date" type="date"/>
        <x-adminlte-input-file name="avatar_url" label="Avatar"/>
        {{-- Register button --}}
        <button type="submit" class="btn btn-block {{ config('adminlte.classes_auth_btn', 'btn-flat btn-primary') }}">
            <span class="fas fa-user-plus"></span>
            {{ __('adminlte::adminlte.register') }}
        </button>

    </form>
@stop

@section('auth_footer')
    <p class="my-0">
        <a href="{{ route('login') }}">
            {{ __('adminlte::adminlte.i_already_have_a_membership') }}
        </a>
    </p>
@stop
