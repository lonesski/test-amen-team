<?php

namespace Database\Seeders;

use App\Models\Domain;
use Illuminate\Database\Seeder;

class DomainSeeder extends Seeder
{
    public function run()
    {
        Domain::create(['host' => 'test-domain-1.test']);
        Domain::create(['host' => 'test-domain-2.test']);
        Domain::create(['host' => 'test-domain-3.test']);
    }
}
