<?php

namespace Database\Seeders;

use App\Models\User;
use Faker\Generator;
use Illuminate\Database\Seeder;

class UserSeeder extends Seeder
{
    /**
     * @var Generator
     */
    private $faker;
    private $defaultPassword;

    public function __construct(Generator $faker)
    {
        $this->faker = $faker;
        $this->defaultPassword = \Hash::make('secret');
    }

    public function run(Generator $faker)
    {
        $this->createUser('admin@gmail.com');

        foreach (range(1, 10) as $i) {
            $this->createUser($this->faker->safeEmail);
        }
    }

    private function createUser(string $email, ?string $password = null): User
    {
        return User::create([
            'name'       => $this->faker->name,
            'email'      => $email,
            'password'   => $password ?? $this->defaultPassword,
            'birth_date' => $this->faker->date(),
            'avatar_url' => $this->faker->url
        ]);
    }
}
